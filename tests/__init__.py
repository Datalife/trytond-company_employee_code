# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_company_employee_code import suite

__all__ = ['suite']
