# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool


class Timesheet(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    employee_code = fields.Function(fields.Char('Employee Code'),
        'get_employee_code', searcher='search_employee_code')

    @classmethod
    def get_employee_code(cls, records, name=None):
        return {r.id: r.employee.code for r in records}

    @classmethod
    def search_employee_code(cls, name, clause):
        return [('employee.code', ) + tuple(clause[1:])]

    @staticmethod
    def order_employee_code(tables):
        pool = Pool()
        Employee = pool.get('company.employee')
        line, _ = tables[None]
        if 'employee' not in tables:
            employee = Employee.__table__()
            tables['employee'] = {
                None: (employee, line.employee == employee.id),
            }
        else:
            employee = tables['employee']
        return Employee.code.convert_order('code', tables['employee'],
            Employee)


class TeamTimesheetEmployee(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-company.employee'

    employee_code = fields.Function(fields.Char('Code'), 'get_employee_code')

    def get_employee_code(self, name=None):
        return self.employee.code

    @fields.depends('employee')
    def on_change_employee(self):
        if self.employee:
            self.employee_code = self.get_employee_code()

    @staticmethod
    def order_employee_code(tables):
        pool = Pool()
        Employee = pool.get('company.employee')
        line, _ = tables[None]
        if 'employee' not in tables:
            employee = Employee.__table__()
            tables['employee'] = {
                None: (employee, line.employee == employee.id),
            }
        else:
            employee = tables['employee']
        return Employee.code.convert_order('code',
            tables['employee'], Employee)


class TeamTimesheet(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet'

    def on_change_team(self):
        super(TeamTimesheet, self).on_change_team()
        for _employee in self.employees:
            _employee.employee_code = _employee.employee.code


class EmployeeTeam(metaclass=PoolMeta):
    __name__ = 'company.employee.team'

    def _get_employee_domain(self, date):
        domain = super(EmployeeTeam, self)._get_employee_domain(date)
        return domain + [('employee.active', '=', True)]
