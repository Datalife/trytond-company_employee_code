datalife_company_employee_code
==============================

The company_employee_code module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-company_employee_code/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-company_employee_code)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
