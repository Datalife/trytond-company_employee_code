# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .employee import Employee, Company
from .timesheet import (Timesheet, TeamTimesheet, TeamTimesheetEmployee,
    EmployeeTeam)


def register():
    Pool.register(
        Employee,
        Company,
        module='company_employee_code', type_='model')
    Pool.register(
        Timesheet,
        TeamTimesheet,
        TeamTimesheetEmployee,
        module='company_employee_code', type_='model',
        depends=['timesheet', 'team_timesheet'])
    Pool.register(
        EmployeeTeam,
        module='company_employee_code', type_='model',
        depends=['company_employee_team'])
